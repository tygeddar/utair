//
//  HumanStepper.swift
//  UTair
//
//  Created by Евгений Елчев on 26.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

public protocol HumanStepperDelegate: class {
    func humanStepper(_ stepper: HumanStepper, shouldChangeFromValue fromValue: Int, toValue: Int) -> Bool
    func humanStepper(_ stepper: HumanStepper, shouldDisableMinusButtonAtValue value: Int) -> Bool
    func humanStepper(_ stepper: HumanStepper, didChangeValue value: Int)
}

@IBDesignable
public final class HumanStepper: XibView {

    public weak var delegate: HumanStepperDelegate?
    
    @IBOutlet public weak var amountLabelView: UILabel!
    @IBOutlet public weak var iconView: UIImageView!
    @IBOutlet public weak var plusButton: UIButton!
    @IBOutlet public weak var kindLabelView: UILabel!
    @IBOutlet public weak var minusButton: UIButton!
    
    private var pStep = 1
    private var pMinimum = 0
    private var pMaximum = 0
    private var buttonsPossibleStateColor = UIColor.blue
    private var buttonsUnpossibleStateActiveColor = UIColor.darkGray
    
    // опять же не знаю на сколько это магическая строка
    // возможно я и не прав, но как по мне пока читаемость и SSOT не нарушены,
    // все хорошо
    public override var intrinsicContentSize: CGSize {
        return CGSize(width: 65, height: 120)
    }

    @IBAction public func plusButtonPressed(_ sender: UIButton) {
        let newValue = amount + pStep
        amount = newValue
    }
    
    @IBAction public func minusButtonPressed(_ sender: UIButton) {
        let newValue = amount - pStep
        amount = newValue
    }
    
    private func animateAmountLabelView() {
        let keyPath = "transform.scale"
        let animation = CASpringAnimation(keyPath: keyPath)
        animation.fromValue = 1.2
        animation.toValue = 1
        animation.duration = 1
        animation.damping = 3
        amountLabelView.layer.add(animation, forKey: animation.keyPath)
    }

}


// MARK: - IBInspectable wrappers
public extension HumanStepper {
    
    @objc @IBInspectable
    public var amount: Int {
        get {
            guard let text = amountLabelView.text, let amount = Int(text) else { return 0 }
            return  amount
        }
        set {
            let shouldChange = delegate?.humanStepper(self, shouldChangeFromValue: amount, toValue: newValue) ?? true
            guard shouldChange else { return }
            
            let shouldDisable = delegate?.humanStepper(self, shouldDisableMinusButtonAtValue: newValue) ?? true
            minusButton.tintColor = shouldDisable ? buttonsUnpossibleStateActiveColor : buttonsPossibleStateColor
            
            delegate?.humanStepper(self, didChangeValue: newValue)
            amountLabelView.text = String(describing: newValue)
            
            animateAmountLabelView()
        }
    }
    
    @objc @IBInspectable
    public var kindLabel: String {
        get {
            return kindLabelView.text ?? ""
        }
        set {
            kindLabelView.text = newValue
        }
    }
    
    @objc @IBInspectable
    public var icon: UIImage? {
        get {
            return iconView.image
        }
        set {
            iconView.image = newValue
        }
    }
    
    @objc @IBInspectable
    public var step: Int {
        get {
            return pStep
        }
        set {
            pStep = min(newValue, 1)
        }
    }
    
    @objc @IBInspectable
    public var minimum: Int {
        get {
            return pMaximum
        }
        set {
            pMaximum = newValue
        }
    }
    
    @objc @IBInspectable
    public var maximum: Int {
        get {
            return pMinimum
        }
        set {
            pMinimum = newValue
        }
    }
    
}
