//
//  LoadXibableView.swift
//  UTair
//
//  Created by Евгений Елчев on 26.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

public protocol LoadXibableView: class {
    var xibView: UIView! {get set}
}

public extension LoadXibableView {
    public func loadViewFromNib() -> UIView? {
        let metaData = type(of: self)
        let bundle = Bundle(for: metaData)
        let nib = UINib(nibName: String(describing: metaData), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view
    }
}

public extension LoadXibableView where Self: UIView {
    public func xibSetup() {
        xibView = loadViewFromNib()
        xibView.frame = bounds
        xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(xibView)
    }
}
