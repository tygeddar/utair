//
//  AirportCell.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

public final class AirportCell: UITableViewCell {
    @IBOutlet public weak var cityLabelView: UILabel!
    @IBOutlet public weak var airportLabelView: UILabel!
    
    public func configure(name: String, country: String) {
        cityLabelView.text = name
        airportLabelView.text = country
    }
    
}
