//
//  ServiceAssembly.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Swinject
import Alamofire

final class ServiceAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(Enviromentable.self) { _ in
            return Enviroment.Development()
        }
        
        container.register(SessionManager.self) { resolver in
            let factory = SessionManagerFactoryImp()
            return factory.default()
        }.inObjectScope(.container)
        
        container.register(CityRouter.self) { resolver in
            let env = resolver.resolve(Enviromentable.self)!
            return CityRouterImp(enviroment: env)
        }

        container.register(CityNetworkService.self) { resolver in
            let router = resolver.resolve(CityRouter.self)!
            let sessionManager = resolver.resolve(SessionManager.self)!
            return CityNetworkServiceImp(router: router, sessionManager: sessionManager)
        }.inObjectScope(.container)
        
        container.register(WeatherApiRouter.self) { resolver in
            let env = resolver.resolve(Enviromentable.self)!
            return WeatherApiRouterImp(enviroment: env)
        }
        
        container.register(WeatherNetworkService.self) { resolver in
            let router = resolver.resolve(WeatherApiRouter.self)!
            let sessionManager = resolver.resolve(SessionManager.self)!
            return WeatherNetworkServiceImp(router: router, sessionManager: sessionManager)
        }.inObjectScope(.container)
        
    }
    
}
