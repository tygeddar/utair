//
//  CGAffineTransform.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

fileprivate let prad = 180.0

// Простой хелпер для поворта в градусах
extension CGAffineTransform {
    
    /// Возвращает transform для вращения
    ///
    /// - Parameter degrees: Угол поворота в градусах
    init(rotationAngleInDegrees degrees: Double) {
        let angle = CGFloat( Double.pi * degrees / prad )
        self.init(rotationAngle: angle)
    }
}
