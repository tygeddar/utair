//
//  BuyTiketsViewImp.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
import UIComponents

final class BuyTiketsViewImp: UIView, BuyTiketsView {
    // Такая же надписать дублируется в другом View, но есть вероятность, что они
    // будут изменяться независимо
    private let defaultCountryLabel = "Все aропорты"
    // градусы для поворота инконки выбора даты отправления
    private let backwardDateIconTransform = ( empty: 0.0, notEmpty: 45.0 )
    // градусы для декоративного поворота кнопки перестановки аэропортов местами
    private let swapAitportButtonAngle = 45.0
    // параметры рамки кнопки выбора даты
    private let backwardDateWrapperViewBorderWidth: ( empty: CGFloat, notEmpty: CGFloat) = (empty: 1.0, notEmpty: 0.0 )
    // длительность анимации поворота инконки выбора даты отправления
    private let backwardDateIconViewAnimationDuration = 0.25
    // параметры сообщения об ошибке
    private var errorViewAttr = ErrorViewAttribute(
        animationDuration: 0.15,
        //FIXME: изменить магические числа на нормальные
        // можно было бы высчитать эти параматеры на основе высоты
        // но iphoneX такое не пройдет
        invisibleTopPosition: -50,
        visibleTopPosition: 40,
        showDuration: 4.0,
        closeTask: nil
    )
    // форматтер для кнопок отображения дат рейсов
    private let flightDateFormat = "dd MMM, E"
    private lazy var flightDateFormater: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = flightDateFormat
        return df
    }()
    @IBOutlet weak var errorView: UIView! { didSet {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideError))
        errorView.addGestureRecognizer(gesture)
    }}
    @IBOutlet weak var errorLabelView: UILabel!
    @IBOutlet weak var errorLabelTopPosition: NSLayoutConstraint!
    private var errorLabelHidden = true
    @IBOutlet weak var departureAitportButton: AirportButton!
    @IBOutlet weak var arrivalAitportButton: AirportButton!
    @IBOutlet weak var swapAitportButton: UIButton! { didSet {
        swapAitportButton.transform = CGAffineTransform(rotationAngleInDegrees: swapAitportButtonAngle)
    }}

    @IBOutlet weak var thitherwardDateView: FlightDate!
    @IBOutlet weak var backwardDateWrapperView: BorderedView!
    @IBOutlet weak var backwardDateView: FlightDate!
    @IBOutlet weak var backwardDateIconView: UIImageView!
    @IBOutlet weak var adultStepperView: HumanStepper!
    @IBOutlet weak var kidStepperView: HumanStepper!
    @IBOutlet weak var babyStepperView: HumanStepper!
    @IBOutlet weak var findFlightButton: BorderedButton! { didSet {
        // Установка тени у кнопки найти рейс
        // так как у нас всего одна кнопка с теню выносить в отдельный класс
        // не стал
        // так же не стал выносить куда либо параметры тени, они хорошо читаются
        findFlightButton.clipsToBounds = false
        findFlightButton.layer.shadowColor = findFlightButton.backgroundColor?.cgColor
        findFlightButton.layer.shadowOpacity = 1
        findFlightButton.layer.shadowRadius = 2
        findFlightButton.layer.shadowOffset = CGSize(width: 0, height: 2)
    }}
    
    func setThitherwardDate(_ date: Date?) {
        let date = date ?? Date()
        thitherwardDateView.dateLabel = flightDateFormater.string(from: date)
    }
    
    func setbackwardDate(_ date: Date?) {
        var stringDate = ""
        
        if let date = date {
            stringDate = flightDateFormater.string(from: date)
        }
        
        backwardDateView.isHidden = stringDate.isEmpty
        backwardDateWrapperView.borderWidth = stringDate.isEmpty ? backwardDateWrapperViewBorderWidth.empty : backwardDateWrapperViewBorderWidth.notEmpty
        
        let backwardDateIconViewAngle = stringDate.isEmpty ? backwardDateIconTransform.empty : backwardDateIconTransform.notEmpty
        UIView.animate(withDuration: backwardDateIconViewAnimationDuration) {
            self.backwardDateIconView.transform = CGAffineTransform(rotationAngleInDegrees: backwardDateIconViewAngle)
        }
        backwardDateView.dateLabel = stringDate
    }
    
    func setDepartureAirport(name: String?, country: String?) {
        let name = name ?? Point.departure.rawValue
        setAirport(name: name, country: country, button: departureAitportButton)
    }
    
    func setArrivalAirport(name: String?, country: String?) {
        let name = name ?? Point.arrival.rawValue
        setAirport(name: name, country: country, button: arrivalAitportButton)
    }
    
    private func setAirport(name: String, country: String?, button: AirportButton) {
        let country = country ?? defaultCountryLabel
        button.setTitle(name, for: .normal)
        button.airportText = country
    }
    
    func showError(withMessage message: String) {
        errorViewAttr.closeTask?.cancel()
        errorLabelView.text = message
        errorLabelHidden = false
        setNeedsUpdateConstraints()
        UIView.animate(withDuration: errorViewAttr.animationDuration) {
            self.layoutIfNeeded()
            UIApplication.shared.delegate?.window??.windowLevel = UIWindowLevelStatusBar + 1
            
        }
        let newTask = DispatchWorkItem { [weak self] in
            self?.hideError()
        }
        errorViewAttr.closeTask = newTask
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.errorViewAttr.showDuration, execute: newTask)
    }
    
    @objc
    func hideError() {
        errorViewAttr.closeTask?.cancel()
        errorLabelHidden = true
        setNeedsUpdateConstraints()
        UIView.animate(withDuration: errorViewAttr.animationDuration) {
            self.layoutIfNeeded()
            UIApplication.shared.delegate?.window??.windowLevel = UIWindowLevelNormal
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        errorLabelTopPosition.constant = errorLabelHidden ? errorViewAttr.invisibleTopPosition : errorViewAttr.visibleTopPosition
    }
}

extension BuyTiketsViewImp {
    private struct ErrorViewAttribute {
        let animationDuration: Double
        let invisibleTopPosition: CGFloat
        let visibleTopPosition: CGFloat
        let showDuration: Double
        var closeTask: DispatchWorkItem?
    }
}
