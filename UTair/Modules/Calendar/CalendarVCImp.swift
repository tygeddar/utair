//
//  CalendarVCImp.swift
//  UTair
//
//  Created by Евгений Елчев on 28.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
import FSCalendar

final class CalendarVCImp: UIViewController, CalendarVC {
    @IBOutlet weak var calendar: FSCalendar! { didSet {
        calendar.dataSource = self
        calendar.delegate = self
    }}
    var minimalDate = Date()
    var onDateSelect: OnDateSelect?

}

extension CalendarVCImp: FSCalendarDataSource {
    func minimumDate(for calendar: FSCalendar) -> Date {
        return minimalDate
    }
}

extension CalendarVCImp: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        onDateSelect?(date)
        dismiss(animated: true)
    }
}
