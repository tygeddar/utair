//
//  SelectAirportBuilder.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
import Swinject

final class SelectAirportBuilder: AbstractModuleBuilder {
    
    @IBOutlet weak var vc: SelectAirportVCImp!
    
    override func configure(assembler: Assembler) {
        if let service = assembler.resolver.resolve(CityNetworkService.self) {
            vc.cityNetworkService = service
        } else {
            assertionFailure()
        }
    }
    
}
