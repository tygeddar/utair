//
//  SelectAirportTableManager.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

protocol SelectAirportTableManager {
    typealias OnAirportSelect = (City) -> Void
    var onAirportSelect: OnAirportSelect? { get set }
    var cityes: [City] { get set }
}

