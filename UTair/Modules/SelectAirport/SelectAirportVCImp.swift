//
//  SelectAirportVCImp.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

final class SelectAirportVCImp: UIViewController, SelectAirportVC {
    private static let networkError = "Список городов недоступен"
    private let searchQueryMinLeght = 2
    
    var keyboadManager = KeyboadManager()
    weak var delegate: SelectAirportVCDelegat?
    var tableManager: SelectAirportTableManager?
    var cityNetworkService: CityNetworkService?
    var customView: SelectAirportView {
        return view as! SelectAirportView
    }
    
    private var state = State(searchText: "", selectedCity: nil, point: .departure, allCity: [])
    private var search = (task: DispatchWorkItem{}, delayTime: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableManager = SelectAirportTableManagerImp(tableView: customView.tableView)
        keyboadManager.scrollView = customView.tableView
        keyboadManager.startKeyboardListen()
        loadCity()
        configureManager()
        customView.searchFeld.addTarget(self, action: #selector(searchTextChanged(sender:)), for: UIControlEvents.editingChanged)
    }
    
    deinit {
        keyboadManager.endKeyboardListen()
    }
    
    private func loadCity() {
        weak var weakSelf = self
        customView.searchActivity.startAnimating()
        cityNetworkService?.search(query: state.searchText, succes: { cityes in
            weakSelf?.state.allCity = cityes
            weakSelf?.tableManager?.cityes = cityes
            weakSelf?.customView.searchActivity.stopAnimating()
        }) { error in
            weakSelf?.customView.searchActivity.stopAnimating()
            weakSelf?.showError(message: SelectAirportVCImp.networkError, animated: true) { _ in
                weakSelf?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func setStartState(point: Point, city: City?) {
        state.point = point
        state.selectedCity = city
        state.searchText = city?.name ?? ""
        customView.set(point: state.point)
        customView.set(city: city)
    }
    
    private func configureManager() {
        tableManager?.onAirportSelect = { [weak self] city in
            guard let point = self?.state.point else {
                assertionFailure()
                return
            }
            self?.delegate?.onAirportSelect(city: city, point: point)
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func searchTextChanged(sender: UITextField) {
        search.task.cancel()
        let searchText = sender.text ?? ""
        state.searchText = searchText.count >= searchQueryMinLeght ? searchText : ""
        let newWearchTask = DispatchWorkItem { [weak self] in
            self?.loadCity()
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + search.delayTime, execute: newWearchTask)
        search.task = newWearchTask
    }
    
}

extension SelectAirportVCImp {
    struct State {
        var searchText: String
        var selectedCity: City?
        var point: Point
        var allCity: [City] = []
    }
}

enum Point: String {
    case arrival = "Куда"
    case departure = "Откуда"
}
