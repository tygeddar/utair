//
//  WeatherStore.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

struct WeatherStore {
    private static let dateFormat = "YYYY.MM.dd"
    private static let dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = dateFormat
        return df
    }()
    
    private let dates: [String]
    private let weathers: [[Weather]]
    
    init(weathers: [Weather]) {
        let sortedWeathers = weathers.reduce([String: [Weather]]()) { result, weather in
            var result = result
            let date = Date(timeIntervalSince1970: weather.date)
            let day = WeatherStore.dateFormatter.string(from: date)
            if result[day] == nil { result[day] = [] }
            result[day]?.append(weather)
            return result
        }
        dates = sortedWeathers.keys.sorted()
        self.weathers = dates.flatMap { sortedWeathers[$0] }
    }
    
    init() {
        dates = []
        weathers = []
    }
    
    subscript(section: Int) -> [Weather] {
        return weathers[section]
    }
    
    subscript(section: Int, row: Int) -> Weather {
        return weathers[section][row]
    }
    
    var sectionCount: Int {
        return weathers.count
    }
    
    func title(forSection section: Int) -> String {
        return dates[section]
    }
}
