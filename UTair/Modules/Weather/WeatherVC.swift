//
//  WeatherVC.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

protocol WeatherVC: class {
    var cityes: (from: City, to: City)? { get set }
}
