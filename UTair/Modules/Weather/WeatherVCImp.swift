//
//  WeatherVCImp.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
//FIXME: На ios 10 titleView в навигейшен баре съезжают в право. С ходу не знаю как это пофиксить, а ковырять уже просто нет времени.
final class WeatherVCImp: UIViewController, WeatherVC {
    
    private static let networkError = "Данные о погоде в данный момент недоступны"
    
    var weatherNetworkService: WeatherNetworkService?
    var tableManager: WeatherTableManager?
    var customView: WeatherView {
        return view as! WeatherView
    }
    
    @IBOutlet weak var fromNavigationLabel: UILabel!
    @IBOutlet weak var toNavigationLabel: UILabel!
    
    private var weathers = ( from: WeatherStore(), to: WeatherStore())
    var cityes: (from: City, to: City)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fromNavigationLabel.text = cityes?.from.name ?? ""
        toNavigationLabel.text = cityes?.to.name ?? ""

        tableManager = WeatherTableManagerImp(tableView: customView.tableView)
        customView.fromTabButton.addTarget(self, action: #selector(fromButtonPressed(sender:)), for: .primaryActionTriggered)
        customView.toTabButton.addTarget(self, action: #selector(toButtonPressed(sender:)), for: .primaryActionTriggered)
        customView.tableView.refreshControl?.addTarget(self, action: #selector(reloadWeather(sender:)), for: .primaryActionTriggered)
        loadWeather()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @objc
    func reloadWeather(sender: UIRefreshControl) {
        loadWeather()
    }
    
    func loadWeather() {
        guard let cityes = self.cityes else {
            assertionFailure()
            return
        }

        weak var weakSelf = self
        weatherNetworkService?.week(city: cityes.from, succes: { weathers in
            weakSelf?.setNewWeather(weathers, direction: .from)
        }, fail: { (error) in
            weakSelf?.showError(message: WeatherVCImp.networkError, animated: true)
        })
        
        weatherNetworkService?.week(city: cityes.to, succes: { weathers in
            weakSelf?.setNewWeather(weathers, direction: .to)
        }, fail: { (error) in
            weakSelf?.showError(message: WeatherVCImp.networkError, animated: true)
        })
    }
    
    private func setNewWeather(_ newWeathers: [Weather], direction: WeatherViewImp.Direction ) {
        let newWeathers = WeatherStore(weathers: newWeathers)
        switch direction {
        case .from: weathers.from = newWeathers
        case .to: weathers.to = newWeathers
        }
        if direction == customView.tabSelect {
            tableManager?.weathers = newWeathers
        }
    }
    
    @objc
    func fromButtonPressed(sender: UIButton) {
        tableManager?.weathers = weathers.from
        customView.tabSelect(sender)
    }
    
    @objc
    func toButtonPressed(sender: UIButton) {
        tableManager?.weathers = weathers.to
        customView.tabSelect(sender)
    }
    
}
