//
//  Enviroment.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

// переменные окружения, на продакшене я обычно завязываю их на окружение из
// xcode, что бы автоматически иметь разные настройки для продакшена и для тестов
protocol Enviromentable {
    var baseApiUrl: URL? { get }
    var weatherBaseApiUrl: URL? { get }
    var weatherApiKey: String { get }
}

// enum  чтобы небыло конструктора
enum Enviroment {}
