//
//  CityNetworkService.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

protocol CityNetworkService {
    typealias AllSucces = ([City]) -> Void
    typealias AllFail = (Error) -> Void
    func search(query: String, succes: @escaping AllSucces, fail: @escaping AllFail)
}
