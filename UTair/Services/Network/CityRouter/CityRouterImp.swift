//
//  CityRouterImp.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Alamofire

struct CityRouterImp: CityRouter {
    
    private let enviroment: Enviromentable
    
    init(enviroment: Enviromentable) {
        self.enviroment = enviroment
    }
    
    /// Поиск городов
    ///
    /// - Parameter query: Имя города или почтовый индекс, может быть пустым
    func search(query: String) -> URLRequestConvertible {
        return Search.init(baseUrl: enviroment.baseApiUrl, query: query)
    }
    
    private struct Search: RequestRoute {
        let baseUrl: URL?
        private let query: String
        
        var method: HTTPMethod = .get
        
        var path: String = "2/cities"
        
        // radius - поиска в милях.
        // Не планируется изменять
        var parameters: Parameters {
            return [
                "radius":3000,
                "query": query
            ]
        }
        
        init(baseUrl: URL?, query: String) {
            self.baseUrl = baseUrl
            self.query = query
        }
    }
    
}
