//
//  WeatherNetworkService.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

protocol WeatherNetworkService {
    typealias Succes = ([Weather]) -> Void
    typealias Fail = (Error) -> Void
    func week(city: City, succes: @escaping Succes, fail: @escaping Fail)
}
